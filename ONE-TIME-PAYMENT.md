# One-time payments mechanism
The one-time payments in this hugo site is the only thing that needs backend PHP code. This to assist in sending the donation and payment data over.

The one-time payment processor we use is called "Mollie". For the PHP API to work, we have to use the Krita foundation API key for it to work. This is not stored in the source code as it can create a security issue with malicious programmers. 

The PHP API code was taken from here for reference:
https://github.com/mollie/mollie-api-php

# Code structure
The one time donation is stored as as shortcode.  You can include it in a content post by typing {{< one-time-donation  >}}.
After the payment is complete, Mollie will redirect the person back to krita.org. There is a one time donation thank you page that exists in the content where people will land on.

