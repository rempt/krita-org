---
title: "發佈 Krita 5.1.3 修正版本"
date: "2022-11-07"
categories: 
  - "news_zh-hk"
  - "officialrelease_zh-hk"
---

今日我哋發佈咗 Krita 5.1.3 版本。依個係一個修正版本，不過亦都因為更新咗一啲相依程式庫而帶嚟咗一啲效能改善。我哋建議用家更新到依個版本。因為我哋喺發佈 5.1.2 版本前嘅最後一刻加入咗更多問題修正，所以需要將版本編號提升至 5.1.3。 （Android 版本因為技術問題而未能及時推出，所以我哋將會提供缺少幾項修正嘅 5.1.2 版本更新。5.1.2 版本冇咗 [BUG:461436](https://bugs.kde.org/show_bug.cgi?id=461436) 同 [BUG:459510](https://bugs.kde.org/show_bug.cgi?id=459510) 嘅修正。）

## 問題修正

（譯者按：由於內容太多而人力資源有限，所以依段唔做完整嘅翻譯，保留返英文原文。）

- For our builds: updated to the latest LittleCMS release candidate for increased performance and correctness. A lot of other dependencies were updated as well. （更新咗部分相依程式庫，其中 LittleCMS 已更新到最新嘅發行候選版本以獲得最新嘅錯誤修正同效能改進。）
- Fix using the Global Menu in the Plasma desktop when using distribution packages. [BUG:408015](https://bugs.kde.org/show_bug.cgi?id=408015) （修正喺使用發行版軟件包時 Plasma 桌面嘅全域選單整合功能。）
- JPEG-XL, HEIF: Fix import/export of HDR Alpha Channels. [BUG:460380](https://bugs.kde.org/show_bug.cgi?id=460380) （JPEG-XL, HEIF: 修正匯入同匯出 HDR Alpha 通道。）
- JPEG-XL, HEIF: Fix clamping of normalized HDR values
- JPEG XL, HEIF: fix saving OOTF removal if it's enabled
- JPEG XL: Fix enabling HLG correction options
- JPEG-XL: Work around linear profiles having an undefined transfer functioｎ
- JPEG-XL: Optimize HDR export
- JPEG-XL: Improve compatibility with the currently experimental JPEG-XL support in Chrome
- Fix handling creating an image from clipboard when the clipboard is empty. [BUG:459800](https://bugs.kde.org/show_bug.cgi?id=459800)
- Fix loading CSV animation files （修正匯入 CSV 動畫功能。）
- Fix paste outside of image boundaries. [BUG:459111](https://bugs.kde.org/show_bug.cgi?id=459111) （修正將剪貼簿內容貼上到喺影像邊界外嘅功能。）
- Fix aliasing of brush tips at small brush sizes. （修正細小筆刷嘅筆尖出現紋理走樣嘅問題。）
- Fix issues with the Line tool. [BUG:460461](https://bugs.kde.org/show_bug.cgi?id=460461) （修正線條工具嘅問題。）
- Fix a crash on selecting and cutting/copying in a new document. [BUG:457475](https://bugs.kde.org/show_bug.cgi?id=457475), BUG:460954 （修正喺新文件中選取並剪下、複製時出現軟件崩潰嘅問題。）
- Android: fix long-press producing a right-click event （Android: 修正長按時觸發右鍵點選嘅問題。）
- Android: handle touch events for Mirror decorations （Android: 令對稱線控制項支援觸控輪入）
- Fix a crash in the pattern fill layer. [BUG:459906](https://bugs.kde.org/show_bug.cgi?id=459906)
- Fix foreground to background color switching of vector objects. [BUG:458913](https://bugs.kde.org/show_bug.cgi?id=458913)
- Fix several issues in TIFF file export. [BUG:459840](https://bugs.kde.org/show_bug.cgi?id=459840)
- Fix issues when changing color theme
- Fix saving files with extreme aspect ratios. [BUG:460624](https://bugs.kde.org/show_bug.cgi?id=460624) （修正儲存長寬比非常窄長嘅影像。）
- Fix issues in the path selection tool （修正路徑選取工具嘅問題。）
- Implement right-click to undo adding a point for the polyline tool （實作喺折線繪製工具中使用右鍵點選以復原加入控制點。）
- Fix copy/paste with animated layers. [BUG:457319](https://bugs.kde.org/show_bug.cgi?id=457319), [BUG:459763](https://bugs.kde.org/show_bug.cgi?id=459763) （修正動畫圖層嘅複製、貼上功能。）
- Make it possible to import more than one bundle at a time
- Make it possible to run Krita on Linux when SELinux is enabled. [BUG:459490](https://bugs.kde.org/show_bug.cgi?id=459490)
- Fix a crash on startup when there is a PSD file with layer styles in the recent files list. [BUG:459512](https://bugs.kde.org/show_bug.cgi?id=459512) （修正當最近開啟清單中包括咗含有圖層樣式嘅 PSD 檔案時，喺啟動 Krita 時出現軟件崩潰嘅問題。）
- Make it possible to run Python scripts if there is no paintable layer. [BUG:459495](https://bugs.kde.org/show_bug.cgi?id=459495)
- Fix the Ten Scripts plugin to actually remember the selected scripts. [BUG:421231](https://bugs.kde.org/show_bug.cgi?id=421231)
- Add an option to PNG export to convert to 8 bit on saving. [BUG:459415](https://bugs.kde.org/show_bug.cgi?id=459415)
- Fix artifacts when hovering over reference images in HiDPI mode. [BUG:441216](https://bugs.kde.org/show_bug.cgi?id=441216)
- Fix thumbnails for pass-through layers being created (they shouldn't...) [BUG:440960](https://bugs.kde.org/show_bug.cgi?id=440960)
- Make the OpenGL workaround available for all platforms. [BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- PSD: fix reading of layer blending ranges. [BUG:459307](https://bugs.kde.org/show_bug.cgi?id=459307)
- Fix a lot of small memory leaks
- Fix copy-paste operation not working after using the move tool. [BUG:458764](https://bugs.kde.org/show_bug.cgi?id=458764)
- Show all loaded python plugins in the Help->System Info dialog
- Show a busy cursor when saving a reference image set. [BUG:427546](https://bugs.kde.org/show_bug.cgi?id=427546)
- Add Document::setModified to the scripting API. [BUG:425066](https://bugs.kde.org/show_bug.cgi?id=425066)
- Fix a crash when trying to save an image with a fill layer. [BUG:459252](https://bugs.kde.org/show_bug.cgi?id=459252)
- Fix a crash when copy/paste a shape or fill layer or a selection mask. [BUG:458115](https://bugs.kde.org/show_bug.cgi?id=458115)
- Fix layer thumbnails when loading a 512x512 PSD file. [BUG:458887](https://bugs.kde.org/show_bug.cgi?id=458887)
- Fix a crash when trying to copy-paste the background layer. [BUG:458890](https://bugs.kde.org/show_bug.cgi?id=458890),[458857](https://bugs.kde.org/show_bug.cgi?id=458857),[458248](https://bugs.kde.org/show_bug.cgi?id=458248),[458941](https://bugs.kde.org/show_bug.cgi?id=458941)
- Don't highlight a layer with a color label on mouse-over. [BUG:459153](https://bugs.kde.org/show_bug.cgi?id=459153)
- Fix creating numbered backups for files with names that contain \[ and \]. [BUG:445500](https://bugs.kde.org/show_bug.cgi?id=445500)
- Add middle handles to perspective transform (patch by Carsten Hartenfels, thanks!)
- Fix dab inaccuracy of Sharpness brushes when outline preview snapping is disabled. [BUG:458361](https://bugs.kde.org/show_bug.cgi?id=458361)
- Fix touchpad gestures on MacOS. [BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會同安裝版本共用設定檔同埋資源。如果想用免安裝版測試並回報 crash 嘅問題，請同時下載偵錯符號 (debug symbols)。

注意：我哋依家唔再提供為 32 位元 Windows 建置嘅版本。

- 64 位元安裝程式：[krita-x64-5.1.3-setup.exe](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.3.zip](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3.zip)
- [偵錯符號 Debug symbols（請解壓到 Krita 程式資料夾入面）](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-dbg.zip)

### Linux

- 64 位元 Linux：[krita-5.1.3-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3-x86_64.appimage)

Linux 版本而家唔使再另外下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你用緊 macOS Sierra 或者 High Sierra，請睇下[依段影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解點樣執行開發者簽署嘅程式。

- macOS 軟件包：[krita-5.1.3.dmg](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.dmg)

### Android

我哋提供嘅 ChomeOS 同 Android 版本仲係**測試版本**。依個版本或可能含有大量嘅 bug，而且仲有部份功能未能正常運作。由於使用者介面仲未改進好，軟件或者須要配合實體鍵盤先可以用到全部功能。Krita 唔啱俾 Android 智能電話用，只係啱平板電腦用，因為使用者介面嘅設計並未為細小嘅螢幕做最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-x86_64-5.1.2-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-x86-5.1.2-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-arm64-v8a-5.1.2-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-armeabi-v7a-5.1.2-release-signed.apk)

### 原始碼

- [krita-5.1.3.tar.gz](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.gz)
- [krita-5.1.3.tar.xz](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.1.3/](https://download.kde.org/stable/krita/5.1.3) 並撳落每個下載檔案嘅「Details」連結以查閱嗰個檔案嘅 MD5 / SHA1 / SHA256 校對碼。

### 數碼簽署

Linux AppImage 以及原始碼嘅 .tar.gz 同 .tar.xz 壓縮檔已使用數碼簽署簽名。你可以由[依度](https://files.kde.org/krita/4DA79EDA231C852B)取得 public key。簽名檔可以喺[依度](https://download.kde.org/stable/krita/5.1.3/)揾到（副檔名為 .sig）。
