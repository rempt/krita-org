---
title: "Teaser of Pepper & Carrot Episode 3"
date: "2022-09-14"
---

The Morevna Project has released a teaser of a new episode of Pepper&Carrot Motion Comic, which is made using Krita and other open-source tools.

{{< youtube Ds7C-RWlFKU >}}

### About Pepper & Carrot
_Pepper & Carrot_ is a webcomic created by David Revoy, who is known for his contributions to Krita and other libre art projects. The sources of the webcomic are available to everyone under CC-BY license and the team of Morevna Project used them to create the animated series.

There are already two episodes of the series released - [Episode 6](https://morevnaproject.org/pepper-and-carrot/episode-6/) in 2017 and [Episode 5](https://morevnaproject.org/pepper-and-carrot/episode-5/) in 2021. The new episode has number 3 and is expected to be released by the end of August.

Right now the Morevna Project team  is looking for sponsors, who wish to get their name listed in final credits. The collected funds will go for producing more episodes (there are 2 more episodes already in production). You can visit [this link](https://morevnaproject.org/pepper-and-carrot/sponsor/) if interested.
