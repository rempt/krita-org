---
title: "New Video: the OTHER brush engines"
date: "2022-10-05"
---

Ramon Miranda has published a new video on Krita's Youtube channel: Making Brushes Part 2: the OTHER brush engines. With a new brush preset pack: https://files.kde.org/krita/extras/FX_01.bundle

{{< youtube W8_lq2r66us >}}


