---
title: "Krita 5.1.3 Released"
date: "2022-11-07"
categories: 
  - "news"
  - "officialrelease"
---

Today we're releasing Krita 5.1.3. This is strictly a bug fix release, but we recommend updating to everyone. There are also performance improvements because we updated some of the libraries we use! Note that we have skipped 5.1.2 because of a last-minute bug fix (with the exception of Android, where we are still on 5.1.2 due to signing trouble, and thus bugs [461436](https://bugs.kde.org/show_bug.cgi?id=461436) and [459510](https://bugs.kde.org/show_bug.cgi?id=459510) are still at large. They will be fixed in the next version).

## Fixes

- For our builds: updated to the latest LittleCMS release candidate for increased performance and correctness. A lot of other dependencies were updated as well.
- Fix using the Global Menu in the Plasma desktop when using distribution packages. [BUG:408015](https://bugs.kde.org/show_bug.cgi?id=408015)
- JPEG-XL, HEIF: Fix import/export of HDR Alpha Channels. [BUG:460380](https://bugs.kde.org/show_bug.cgi?id=460380)
- JPEG-XL, HEIF: Fix clamping of normalized HDR values
- JPEG XL, HEIF: Fix saving OOTF removal if it's enabled
- JPEG XL: Fix enabling HLG correction options
- JPEG-XL: Work around linear profiles having an undefined transfer function
- JPEG-XL: Optimize HDR export
- JPEG-XL: Improve compatibility with the currently experimental JPEG-XL support in Chrome
- Fix handling creating an image from clipboard when the clipboard is empty. [BUG:459800](https://bugs.kde.org/show_bug.cgi?id=459800)
- Fix loading CSV animation files
- Fix paste outside of image boundaries. [BUG:459111](https://bugs.kde.org/show_bug.cgi?id=459111)
- Fix aliasing of brush tips at small brush sizes.
- Fix issues with the Line tool. [BUG:460461](https://bugs.kde.org/show_bug.cgi?id=460461)
- Fix a crash on selecting and cutting/copying in a new document. [BUG:457475](https://bugs.kde.org/show_bug.cgi?id=457475), BUG:460954
- Android: fix long-press producing a right-click event
- Android: handle touch events for Mirror decorations
- Fix a crash in the pattern fill layer. [BUG:459906](https://bugs.kde.org/show_bug.cgi?id=459906)
- Fix foreground to background color switching of vector objects. [BUG:458913](https://bugs.kde.org/show_bug.cgi?id=458913)
- Fix several issues in TIFF file export. [BUG:459840](https://bugs.kde.org/show_bug.cgi?id=459840)
- Fix issues when changing color theme
- Fix saving files with extreme aspect ratios. [BUG:460624](https://bugs.kde.org/show_bug.cgi?id=460624)
- Fix issues in the path selection tool
- Implement right-click to undo adding a point for the polyline tool
- Fix copy/paste with animated layers. [BUG:457319](https://bugs.kde.org/show_bug.cgi?id=457319), [BUG:459763](https://bugs.kde.org/show_bug.cgi?id=459763)
- Make it possible to import more than one bundle at a time
- Make it possible to run Krita on Linux when SELinux is enabled. [BUG:459490](https://bugs.kde.org/show_bug.cgi?id=459490)
- Fix a crash on startup when there is a PSD file with layer styles in the recent files list. [BUG:459512](https://bugs.kde.org/show_bug.cgi?id=459512)
- Make it possible to run Python scripts if there is no paintable layer. [BUG:459495](https://bugs.kde.org/show_bug.cgi?id=459495)
- Fix the Ten Scripts plugin to actually remember the selected scripts. [BUG:421231](https://bugs.kde.org/show_bug.cgi?id=421231)
- Add an option to PNG export to convert to 8 bit on saving. [BUG:459415](https://bugs.kde.org/show_bug.cgi?id=459415)
- Fix artifacts when hovering over reference images in HiDPI mode. [BUG:441216](https://bugs.kde.org/show_bug.cgi?id=441216)
- Fix thumbnails for pass-through layers being created (they shouldn't...) [BUG:440960](https://bugs.kde.org/show_bug.cgi?id=440960)
- Make the OpenGL workaround available for all platforms. [BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- PSD: fix reading of layer blending ranges. [BUG:459307](https://bugs.kde.org/show_bug.cgi?id=459307)
- Fix a lot of small memory leaks
- Fix copy-paste operation not working after using the move tool. [BUG:458764](https://bugs.kde.org/show_bug.cgi?id=458764)
- Show all loaded python plugins in the Help->System Info dialog
- Show a busy cursor when saving a reference image set. [BUG:427546](https://bugs.kde.org/show_bug.cgi?id=427546)
- Add Document::setModified to the scripting API. [BUG:425066](https://bugs.kde.org/show_bug.cgi?id=425066)
- Fix a crash when trying to save an image with a fill layer. [BUG:459252](https://bugs.kde.org/show_bug.cgi?id=459252)
- Fix a crash when copy/paste a shape or fill layer or a selection mask. [BUG:458115](https://bugs.kde.org/show_bug.cgi?id=458115)
- Fix layer thumbnails when loading a 512x512 PSD file. [BUG:458887](https://bugs.kde.org/show_bug.cgi?id=458887)
- Fix a crash when trying to copy-paste the background layer. [BUG:458890](https://bugs.kde.org/show_bug.cgi?id=458890),[458857](https://bugs.kde.org/show_bug.cgi?id=458857),[458248](https://bugs.kde.org/show_bug.cgi?id=458248),[458941](https://bugs.kde.org/show_bug.cgi?id=458941)
- Don't highlight a layer with a color label on mouse-over. [BUG:459153](https://bugs.kde.org/show_bug.cgi?id=459153)
- Fix creating numbered backups for files with names that contain \[ and \]. [BUG:445500](https://bugs.kde.org/show_bug.cgi?id=445500)
- Add middle handles to perspective transform (patch by Carsten Hartenfels, thanks!)
- Fix dab inaccuracy of Sharpness brushes when outline preview snapping is disabled. [BUG:458361](https://bugs.kde.org/show_bug.cgi?id=458361)
- Fix touchpad gestures on MacOS. [BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.3-setup.exe](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.3.zip](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.1.3/krita-x64-5.1.3-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.3-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.3.dmg](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-x86_64-5.1.2-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-x86-5.1.2-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-arm64-v8a-5.1.2-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.2/krita-armeabi-v7a-5.1.2-release-signed.apk)

### Source code

- [krita-5.1.3.tar.gz](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.gz)
- [krita-5.1.3.tar.xz](https://download.kde.org/stable/krita/5.1.3/krita-5.1.3.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.1.3/](https://download.kde.org/stable/krita/5.1.3) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.1.3/) (filenames ending in .sig).
