---
title: "Going Wild With Animation"
date: "2015-11-28"
---

We knew the animation plugin was something artists all over the world were really waiting for... What we hadn't expected was the flood of cool little animations that suddenly appeared everywhere! Let's take a look at a selection of them! And in another way, the release also helped find some issues. First: if onion skinning doesn't work for you, check that you're not trying to onion skin a completely opaque layer. Yes -- white is also opaque! The best setup: create a white background layer, but paint on a transparent layer above that. That's Krita's default setup in any case. For some people with some combinations of graphics cards and drivers, the Instant Preview doesn't work. There's not much we can do about that, but we do need your reports! And finally, a couple of real bugs surfaced, and we'll look at that next.

But here's the animation gallery! 

{{< youtube oxTPZXidvSI >}}

 By Timothee Giet

![Toothless dargon turning head](images/posts/2015/tumblr_nyddn9aI1z1qk9kjpo1_500.gif) and ![Horse galloping](images/posts/2015/tumblr_nydc17gN9k1qk9kjpo1_500.gif) by JackTheVulture on tumblr.

[Falling ball](https://pbs.twimg.com/tweet_video/CUqCk9aUsAA8x_C.mp4) by はまの ‏@HaMoO0NoO0 on twitter.

[exploding sparkles](https://pbs.twimg.com/tweet_video/CUqPxMtWcAAm4mE.mp4) by Nahuel Belich on twitter

[Dancing Gronky](https://pbs.twimg.com/tweet_video/CUqdKyFUcAAQivB.mp4) and [did anyone say bitmap animation](https://pbs.twimg.com/tweet_video/CUqR9ECUAAA7k61.mp4) by SJ Bennet on twitter

[Growing Tree](https://pbs.twimg.com/tweet_video/CUqUgRhVAAAzxCU.mp4) by 雑賀屋鳶 ‏@tomB_saikaya on twitter.

[walkcycle](https://pbs.twimg.com/tweet_video/CUr74xeW4AAsQz5.mp4) by JeffersonSN/Llama guy on twitter

[walking dogman](https://pbs.twimg.com/tweet_video/CUsXmjnXIAAC5c5.mp4) by Godzillu on twitter

{{< youtube PX8mW_w4jb8 >}}

 by Степан Крани on youtube

{{< youtube JuX6y82FsW0 >}}

 by Немитько Николай on youtube

{{< youtube bi5sqtnu8FY >}}

 By Dileep N on youtube

![Test gif of walking pruple alien man](https://giphy.com/gifs/walk-wheel-krita-d2Z0VzhaSMGkR5HG) by Benjamin Mitchley on twitter

[mouse hiding under pillows](https://pbs.twimg.com/tweet_video/CUsJw3jWEAAmNk6.mp4) and [bird flying on tree](https://pbs.twimg.com/tweet_video/CUxt-XeW4AAQqDN.mp4) by Vincent Sautter on twitter

{{< youtube VCRdnUSpaQs >}}

 By Popescu Sorin on youtube

[hand smashing block/alarm clock](https://pbs.twimg.com/tweet_video/CU3RNs5VEAApStO.mp4) By Pablo Mendoza on twitter

{{< youtube kFmx8p6I9Y0 >}}

 by Laura Sulter on youtube/twitter
