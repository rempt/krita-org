---
title: "Krita Monthly Update – Edition 15"
date: "2024-05-07"
---

It is time for the monthly news update brought to you by the [Krita-promo](https://krita-artists.org/g/krita-promo) team. Let us take a look at the highlights of krita community and development for this month.

## Development report

* Our users on chromebooks faced a nasty bug which crashed krita on startup. So we made a  5.2.2.1 hotfix release for Android Play Store only to fix this bug. It also contains other fixes from the stable branch, but be warned there is a known crash regression with importing audio.

* A proper 5.2.3 release for all supported platforms will be made as soon as possible, hopefully in the next few weeks.

* At the time of writing, nightly builds for macOS are still blocked by a signing-related issue. Once that is resolved, automated builds for all supported platforms will be up and running again. That is the culmination of months of work by lead developer Dmitry Kazakov, together with macOS developer Iván Yossi, Android developer Sharaf Zaman, Windows contributor Simon Ra, and others, in a refactor of Krita’s build system.

* [Feature Request: Palette in Toolbar](https://krita-artists.org/t/palette-in-toolbar/30016) has been marked “solved” by freyalupen’s most recently merged code. [Add docker box toolbar widget](https://invent.kde.org/graphics/krita/-/merge_requests/2104/) allows the user to add any docker to the toolbar in a temporary popup widget similar to the “choose brush preset” one in Painter’s Tools.

* A problem with certain RBGA brushes has been solved and will be part of the next release. Users were experiencing lagging and freezing when accessing these brushes. The thread makes an interesting read as it’s a “live” look at an issue being revealed and it shows how helpful it is when users conduct testing. You can read the thread [here](https://krita-artists.org/t/rotating-light-brushtips-wip/64930/207?u=sooz).

* Ken_Lo has been accepted as a student for Google Summer of Code, to work on[ pixel perfect hand-drawn lines](https://krita-artists.org/t/pixel-perfect-line-setting-for-pixel-art-brushes/42629).

* In addition to various recorder related fixes by @freyalupen, the FFmpeg profiles in the recorder docker are improved by @Ralek. We congratulate @Ralek on their first contribution to Krita.

* When entering canvas-only mode, the document used to jump abruptly and reposition itself. @YRH helped in [solving](https://invent.kde.org/graphics/krita/-/merge_requests/2097) this issue.

* Deif_Lou has [improved](https://invent.kde.org/graphics/krita/-/merge_requests/2125) performance of the fill tool making it faster.

* Ken Lo [added](https://invent.kde.org/graphics/krita/-/merge_requests/2076) an option in the settings to pick default export file type.

![default file type option for the export](images/posts/2024/screenshot-default-filetype-export.png)

* Grum999 has looked into improving Krita’s API for python plugins and as a start, chose to [implement a scratchpad API](https://invent.kde.org/graphics/krita/-/merge_requests/2087) that adds functionality to the scratchpad.

* Emir Sari sent [patches](https://invent.kde.org/graphics/krita/-/merge_requests/2099) to help Krita build on [Haiku OS](https://www.haiku-os.org/).


## Community report

### March 2024 Monthly Art Challenge

The April Monthly Art Challenge, Animal Curiosity, inspired submissions from 26 artists. @jimplex was voted the winner with this creative piece:
Firefly by [jimplex](https://krita-artists.org/u/jimplex/activity/portfolio)
![Firefly by Jimplex](images/posts/2024/monthly-edition-15-featured-post-3.jpeg)

The theme for the May 2024 challenge is “reflection.” You can get all the details [here](https://krita-artists.org/t/monthly-art-challenge-may-2024-reflection/90495). We already have some ideas and pre-work flying around in the [discussion and WIP thread](https://krita-artists.org/t/monthly-art-challenge-wips-and-discussion-thread-may-2024/90504). Have a look – something might inspire your creativity.

## Featured artwork

Krita-Artists members nominated 9 images for the featured artwork banner. When the mid-month poll ended, these are the 5 that won a place on the banner. All 5 will be entered into the Best of Krita-Artists 2024 competition next January.

Cabin in the woods-RH by [Rohit Hela](https://krita-artists.org/u/rohithela/activity/portfolio)

![Cabin in the woods by Rohit Hela](images/posts/2024/monthly-edition-15-featured-post-6.jpeg)

Detailed Portrait by [denjay5](https://krita-artists.org/u/denjay5/activity/portfolio)

![Detailed Portrait by denjay5](images/posts/2024/monthly-edition-15-featured-post-2.jpeg)

Nier Automata by [IvanGilbertt](https://krita-artists.org/u/ivangilbertt/activity/portfolio)

![Nier Automata by IvanGilbertt](images/posts/2024/monthly-edition-15-featured-post-5.jpeg)

Alien Senator by [DavB](https://krita-artists.org/u/davb/activity/portfolio)

![Alien Senator by DavB](images/posts/2024/monthly-edition-15-featured-post-4.jpeg)

My uni project by [smollbirb](https://krita-artists.org/u/smollbirb/activity/portfolio)

![My uni project by smollbirb](images/posts/2024/monthly-edition-15-featured-post-1.jpeg)

[Nominations for the April/May poll](https://krita-artists.org/t/best-of-krita-artists-april-may-2024-nomination-submissions-thread/89210) are open until May 11, 2024.


## Noteworthy plugin

**Blender-Krita link plugin for texture editing by [heisenshark](https://krita-artists.org/u/heisenshark)**

This plugin has a fresh update that the author describes as a “big overhaul of how the plugin works.” Check out the thread on Krita-artists.org [here](https://krita-artists.org/t/blender-krita-link-plugin-for-texture-editing-in-krita/83980/12).
![blender-krita link](images/posts/2024/plugin-blender-to-krita.png)

## Tutorial of the month

Krita’s newest tutorial by Ramon Miranda features an interview with [Rakurri](https://krita-artists.org/u/rakurri/activity/portfolio), the creator of [Rakurri’s brush pack](https://krita-artists.org/t/rakurri-brush-set-v2-free-krita-brushes/33709) containing more than 200 brushes made just for Krita. Ramon demonstrates his favorite ones such Glow FX, Liquid Bristle and the vegetation brushes.

{{< youtube 9kOUP4uOOx0 >}}


## Notable changes in code

This section has been compiled by [freyalupen](https://krita-artists.org/u/freyalupen/summary).
Apr 3 - May 2, 2024

---

### Stable branch (5.2.2+):
**Bugfixes:**

* **Android: x86_64** -  Fix crash on startup on x86_64, which was being encountered on Chromebooks. ([BUG:485707](https://bugs.kde.org/485707)) ([merge request, Sharaf Zaman](https://invent.kde.org/graphics/krita/-/merge_requests/2131))
* **Brush Presets** -  Speedup loading of GBR brushtips, and brush presets with embedded resources. ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2112))
* **Brush Engines** -  Fix animated brushtips' sequence order in the Pixel Engine. This also introduces a small change to Color Smudge animated brushtips, where the first dab is now used for getting smudge information instead of painting. ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2116))
* **Color Sampler** -  Don't reset redo when doing color picking. ([BUG:485910](https://bugs.kde.org/485910))([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2133))
* **Calligraphy Tool** -  Fix "Use tablet angle" Calligraphy Tool ignoring tilt. ([BUG:485709](https://bugs.kde.org/485709)) ([merge request, George Gianacopoulos](https://invent.kde.org/graphics/krita/-/merge_requests/2121))
* **Grids** -  Fix Grid visual glitches with Solid subdivision style. ([BUG:484889](https://bugs.kde.org/484889)) ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2106))
* **Transform Tool** -  Fix bug where transformed image had aliased left edge. ([BUG:484677](https://bugs.kde.org/484677)) ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/5ae24a5650cccd807a5c0deebc76cde2b493531e))
* **File Formats: XCF** -  Show an error when trying to import unsupported version 3 xcf files instead of loading an empty canvas. ([BUG:485420](https://bugs.kde.org/485420)) ([commit, Halla Rempt](https://invent.kde.org/graphics/krita/-/commit/1129754df83ad77e7514aec5ee0053a5ab0ac991))

**Nightly build regression bugfixes:**

* **Layer Stack** -  Fix wrong layer being active on opening document. In the case of single-layer documents, no layer was active, which caused crashes under some circumstances. ([BUG:480718](https://bugs.kde.org/480718)) ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2115/))

---

### Unstable branch (5.3.0-prealpha):

**Features:**

* **Toolbars, Shortcuts** -  Add Docker Box action that shows a docker in a temporary box, which can be added to a toolbar or assigned to shortcut. ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2104))
* **Canvas Input Shortcuts** -  Add new Tool Invocation action, "Activate with Other Color". This can be bound to a key+mousebutton, where holding those keys will cause, for instance, the Freehand Brush to paint with the background instead of foreground color. ([merge request, ziplantil](https://invent.kde.org/graphics/krita/-/merge_requests/2105))

**Bugfixes:**

* **Fill Tools** -  Improve performance of fill tools, so that time taken depends on fill size and not image size. Filling also now starts on mouse-press and not mouse-release. ([merge request, Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests/2125))
* **Brush Engines** -  Fix assert on selecting RGBA brushes in incorrect format. ([BUG:484115](https://bugs.kde.org/484115)) ([commit, Agata Cacko](https://invent.kde.org/graphics/krita/-/commit/13fe82adbb67fa3234ff9bbfb1ba2a7820b099fe))
* **Transform Tool** -  Fix free transform inverted rotation when flipped over in perspective. ([merge request, Stuffins](https://invent.kde.org/graphics/krita/-/merge_requests/2114))
* **Transform Tool** -  Fix Vector Layer scale+shear transforms behaving incorrectly. ([BUG:485689](https://bugs.kde.org/485689)) ([merge request, Stuffins](https://invent.kde.org/graphics/krita/-/merge_requests/2122))
* **Selection Tools** -  Fix issue making selections on color-labeled reference selections. ([BUG:486419](https://bugs.kde.org/486419)) ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/66dd431ca6d548ca7e169cc7c66568df3243f417))
* **General** -  Make Export's default file type changeable, in Configure->General->File Handling. ([merge request, Ken Lo](https://invent.kde.org/graphics/krita/-/merge_requests/2076))
* **Recorder Docker** -  Reworked default recorder docker FFmpeg profiles. If canvas size changes during recording, the export profiles now keep aspect instead of stretching ([BUG:429326](https://bugs.kde.org/429326)). Issues with resize, result preview, and extend result are avoided ([BUG:455006](https://bugs.kde.org/455006), [BUG:450790](https://bugs.kde.org/450790), [BUG:485515](https://bugs.kde.org/485515), [BUG:485514](https://bugs.kde.org/485514)). For MP4, detect whether openh264 or libx264 is present instead of using separate profiles. ([merge request, Ralek Kolemios](https://invent.kde.org/graphics/krita/-/merge_requests/2124))
* **Recorder Docker** -  Make sure Recorder export duration label updates when FPS is changed. ([merge request, Ken Lo](https://invent.kde.org/graphics/krita/-/merge_requests/2088))
* **Comics Manager Docker** -  Fix Comic export crop margins' right and bottom being swapped. ([merge request, Bob Raskoph](https://invent.kde.org/graphics/krita/-/merge_requests/2126))
* **Scripting** -  Fix waitForDone() not waiting for transform mask update. ([BUG:485053](https://bugs.kde.org/485053)) ([merge request, Agata Cacko](https://invent.kde.org/graphics/krita/-/merge_requests/2110))

**Nightly build regression bugfixes:**

* **Recorder Docker** -  Fix automatic timelapse recording checkbox. ([BUG:482378](https://bugs.kde.org/482378)) ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2108))
* **Recorder Docker** -  Fix recording stopping when another document was opened. ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2118))

---

**These changes are made available for testing in the following Nightly builds:**

* **Stable "Krita Plus" (5.2.2+)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) [not yet] - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64))
* **Unstable "Krita Next" (5.3.0-prealpha)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) [unsigned currently] - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64))

{{< support-krita-callout >}}
