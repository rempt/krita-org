---
title: "Krita 5.2.5 Released!"
date: "2024-09-25"
categories: 
  - "news"
  - "officialrelease"
---

Krita 5.2.5 has a [critical bug (493774)](https://bugs.kde.org/show_bug.cgi?id=493774), please dowload [Krita 5.2.6](/posts/2024/krita-5-2-6-released) instead!

Krita 5.2.5 is here, bringing over 50 bugfixes since 5.2.3 (5.2.4 was a Windows-specific hotfix). Major fixes have been done to audio playback, transform mask calculation and more!

In addition to the core team, special thanks to Maciej Jesionowski, Ralek Kolemios, Freya Lupen, Michael Genda, Rasyuqa A. H., Simon Ra and Sam James for a variety of fixes!

## Changes since 5.2.3:



- Correctly adjust audio playback when animation framerate is changed.
- Fix no layer being activated on opening a document ([Bug 490375](https://bugs.kde.org/show_bug.cgi?id=490375))
- [mlt] Fix incorrect usage of get_frame API ([Bug 489146](https://bugs.kde.org/show_bug.cgi?id=489146))
- Fix forbidden cursor blinking when switching tools with shortcuts ([Bug 490255](https://bugs.kde.org/show_bug.cgi?id=490255))
- Fix conflicts between mouse and touch actions requested concurrently ([Bug 489537](https://bugs.kde.org/show_bug.cgi?id=489537))
- Only check for the presence of bt2020PQColorSpace on Windows ([Bug 490301](https://bugs.kde.org/show_bug.cgi?id=490301))
- Run macdeployqt after searching for missing libs ([Bug 490181](https://bugs.kde.org/show_bug.cgi?id=490181))
- Fix crash when deleting composition
- Fix scaling down image with 1px grid spacing ([Bug 490898](https://bugs.kde.org/show_bug.cgi?id=490898))
- Fix layer activation issue when opening multiple documents ([Bug 490843](https://bugs.kde.org/show_bug.cgi?id=490843))
- Make clip-board pasting code a bit more robust ([Bug 490636](https://bugs.kde.org/show_bug.cgi?id=490636))
- Fix a number of issues with frame generation ([Bug 486417](https://bugs.kde.org/show_bug.cgi?id=486417))
- A number of changes related to qt6 port changes.
- Fix black canvas appearing when "Limit animation frame size" is active ([Bug 486417](https://bugs.kde.org/show_bug.cgi?id=486417))
- WebP: fix colorspace export issue when dithering is enabled ([Bug 491231](https://bugs.kde.org/show_bug.cgi?id=491231))
- WebP: preserve color profile on export if color model is RGB(A)
- Fix layer selection when a layer was removed while view was inactive
- Fix On-Canvas Brush Editor's decimal sliders ([Bug 447800](https://bugs.kde.org/show_bug.cgi?id=447800), [Bug 457744](https://bugs.kde.org/show_bug.cgi?id=457744))
- Make sure file layers are updated when image size or resolution changes ([Bug 467257](https://bugs.kde.org/show_bug.cgi?id=467257), [Bug 470110](https://bugs.kde.org/show_bug.cgi?id=470110))
- Fix Advanced Export of the image with filter masks or layer styles ([Bug 476980](https://bugs.kde.org/show_bug.cgi?id=476980))
- Avoid memory leak in the advanced export function
- Fix mipmaps not being regenerated after transformation was finished or cancelled ([Bug 480973](https://bugs.kde.org/show_bug.cgi?id=480973))
- [Gentoo] Don't use xsimd::default_arch in the pixel scaler code
- KisZug: Fix ODR violation for map_*
- Fix a crash in Filter Brush when changing the filter type ([Bug 478419](https://bugs.kde.org/show_bug.cgi?id=478419))
- PSD: Don't test reference layer for homogenous check ([Bug 492236](https://bugs.kde.org/show_bug.cgi?id=492236))
- Fix an assert that should have been a safe assert ([Bug 491665](https://bugs.kde.org/show_bug.cgi?id=491665))
- Set minimum freetype version to 2.11 ([Bug 489377](https://bugs.kde.org/show_bug.cgi?id=489377))
- Set Krita Default on restoring defaults ([Bug 488478](https://bugs.kde.org/show_bug.cgi?id=488478))
- Fix loading translated news ([Bug 489477](https://bugs.kde.org/show_bug.cgi?id=489477))
- Make sure that older files with simple transform masks load fine & Fix infinite loop with combination of clone + transform-mask-on-source ([Bug 492320](https://bugs.kde.org/show_bug.cgi?id=492320))
- Fix more cycling updates in clone/transform-masks combinations ([Bug 443766](https://bugs.kde.org/show_bug.cgi?id=443766))
- Fix incorrect threaded image access in multiple clone layers ([Bug 449964](https://bugs.kde.org/show_bug.cgi?id=449964))
- TIFF: Ignore resolution if set to 0 ([Bug 473090](https://bugs.kde.org/show_bug.cgi?id=473090))
- Specific Color Selector: Update labels fox HSX ([Bug 475551](https://bugs.kde.org/show_bug.cgi?id=475551))
- Specific Color Selector: Fix RGB sliders changing length ([Bug 453649](https://bugs.kde.org/show_bug.cgi?id=453649))
- Specific Color Selector: Fix float slider step 1 -> 0.01 
- Specific Color Selector: Fix holding down spinbox arrows ([Bug 453366](https://bugs.kde.org/show_bug.cgi?id=453366))
- Fix clone layers resetting the animation cache ([Bug 484353](https://bugs.kde.org/show_bug.cgi?id=484353))
- Fix an assert when trying to activate an image snapshot ([Bug 492114](https://bugs.kde.org/show_bug.cgi?id=492114))
- Fix redo actions to appear when undoing juggler-compressed actions ([Bug 491186](https://bugs.kde.org/show_bug.cgi?id=491186))
- Update cache when cloning perspective assistants ([Bug 493185](https://bugs.kde.org/show_bug.cgi?id=493185))
- Fix a warning on undoing flattening a group ([Bug 474122](https://bugs.kde.org/show_bug.cgi?id=474122))
- Relink clones to the new layer when flattening ([Bug 476514](https://bugs.kde.org/show_bug.cgi?id=476514))
- Fix onion skins rendering on layers with a transform masks ([Bug 457136](https://bugs.kde.org/show_bug.cgi?id=457136))
- Fix perspective value for hovering pixel
- Fix Move and Transform tool to work with Pass-Through groups ([Bug 457957](https://bugs.kde.org/show_bug.cgi?id=457957))
- JPEG XL: Export: implement streaming encoding and progress reporting
- Deselect selection when pasting from the clipboard ([Bug 459162](https://bugs.kde.org/show_bug.cgi?id=459162))

