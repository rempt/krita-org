---
title: "Warning: Krita 5.2.6 beta on Android is currently broken"
date: "2024-10-29"
---
On releasing the latest version of Krita in our Android/ChromeOS beta program, we discovered, too, late that there was a problem that could prevent Krita from starting.

Since the Google Play Store Console does not allow revering a release to an earlier version, we are now urgently working on a fix which we will release as soon as possible.

Our apologies for the inconvience.

The currentl nightly builds for Android work again, with some limitations:

* take care removing the store version of Krita does not remove the application data: your artwork could be lost.
* in the Nightly builds you need to install any brush presets separately

You can get the night builds here: [Krita Next Nightly Builds](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/). You will need to select the package that is right for the architecture of your device.

Installing the nightly builds requires enabling developer mode on your device and needs considerable technical insight. 

If you do not feel comfortable with this, please wait until the new official release lands in the play store in a about two days.