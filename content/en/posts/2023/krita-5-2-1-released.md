---
title: "Krita 5.2.1 Released"
date: "2023-11-02"
categories: 
  - "news"
  - "officialrelease"
---

Krita 5.2.1 has just been released. This is a bugfix release for the stable Krita 5.2.0 release. Check out [release notes](en/release-notes/krita-5-2-release-notes) for everything that was new in Krita 5.2.0.

There are over fifty fixes in Krita 5.2.1, so everyone is encouraged to update!

![](images/posts/2023/5.2.0-1.png)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.1-setup.exe](https://download.kde.org/stable/krita/5.2.1/krita-x64-5.2.1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.1.zip](https://download.kde.org/stable/krita/5.2.1/krita-x64-5.2.1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.2.1/krita-x64-5.2.1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.1-x86\_64.appimage](https://download.kde.org/stable/krita/5.2.1/krita-5.2.1-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.1.dmg](https://download.kde.org/stable/krita/5.2.1/krita-5.2.1.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.1/krita-x86_64-5.2.1-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.2.1/krita-x86-5.2.1-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.1/krita-arm64-v8a-5.2.1-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.2.1/krita-armeabi-v7a-5.2.1-release-signed.apk)

### Source code

- [krita-5.2.1.tar.gz](https://download.kde.org/stable/krita/5.2.1/krita-5.2.1.tar.gz)
- [krita-5.2.1.tar.xz](https://download.kde.org/stable/krita/5.2.1/krita-5.2.1.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.2.1/](https://download.kde.org/stable/krita/5.2.1) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. The signatures are [here](https://download.kde.org/stable/krita/5.2.1/) (filenames ending in .sig).

{{< support-krita-callout >}}
