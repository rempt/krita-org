---
title: "Sponsors"
date: "2018-05-17"
layout: simple
---

## Sponsorship

We welcome sponsors for Krita! If your company or organization would like to sponsor Krita, either for a one-off event or on a monthly basis, contact Halla Rempt at **foundation (at) krita (dot) org** for more information

### Current Sponsors

{{< sponsor-item imageUrl="images/pages/EpicMegaGrants_Badge_Dark320_reoLLjw.png" alt="Epic Mega Grants" >}}

Epic supported Krita in 2019 through the [Mega Grants program](https://www.unrealengine.com/en-US/megagrants) with $25,000.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/sponsors/intel-logo.png" alt="Intel" >}}

[Intel](https://www.intel.com)  became a corporate gold sponsor in 2022.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/sponsors/broken-rules-logo.png" alt="Broken Rules Games" >}}

[Broken Rules](https://brokenrul.es)  sponsored Krita in 2023 with €5,000.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/gamechuck-logo.png" alt="Gamechuck" >}}

[Gamechuck](https://game-chuck.com/) has sponsored Krita in 2019 and uses Krita for creating assets for their games.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/FOSSHUB_logo.png" alt="FOSSHUB" >}}

FossHub sponsors the Krita Foundation with a monthly donation from 2019 on.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/agile_coach.jpg" alt="AGILE.COACH" >}}

[Agile Coach](https://agile.coach/) sponsored the Krita Foundation in 2018

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/PIAlogoupdated_final.png" alt="Private Internet Access" >}}

Private Internet access donated £20,000 in 2017.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/xoio_logo.png" alt="XOIO" >}}

[xoio.de](xoio.de) sponsored the Krita Foundation in 2017.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/pages/asifa-logo-2018.png" alt="ASIFA" >}}

[ASIFA-Hollywood](http://www.asifa-hollywood.org/) sponsored the Krita Foundation in 2017 and 2018.

{{< /sponsor-item >}}
