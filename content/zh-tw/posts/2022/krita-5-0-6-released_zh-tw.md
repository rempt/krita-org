---
title: "發佈 Krita 5.0.6 修正版本"
date: "2022-04-27"
categories: 
  - "news_zh-tw"
  - "officialrelease_zh-tw"
---

今天我們發佈了 Krita 5.0.6。這個修正版本修正了兩項會導致軟體崩潰的問題：

- 在使用向量圖層或向量選取區域時，因為使用復原操作而引致軟體崩潰：[BUG:447985](https://bugs.kde.org/show_bug.cgi?id=447985)
- 刪除帶有動態透明度遮罩的向量圖層時發生軟體崩潰：[BUG:452396](https://bugs.kde.org/show_bug.cgi?id=452396)

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是自由、免費及開源的專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或[購買教學影片](https://krita.org/en/shop/)支持我們吧！得到您們的熱心支持，我們才能夠讓核心開發者全職為 Krita 工作。

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.0.6-setup.exe](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-setup.exe)
- 64 位元免安裝版：[krita-x64-5.0.6.zip](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之中）](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-dbg.zip)

### Linux

- 64 位元 Linux AppImage：[krita-5.0.6-x86\_64.appimage](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 套件：[krita-5.0.6.dmg](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86_64-5.0.6-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86-5.0.6-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-arm64-v8a-5.0.6-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-armeabi-v7a-5.0.6-release-signed.apk)

### 原始碼

- [krita-5.0.6.tar.gz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.gz)
- [krita-5.0.6.tar.xz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.0.6/](https://download.kde.org/stable/krita/5.0.6) 並點選各下載檔案的「Details」連結以查閱該檔案的 MD5 / SHA1 / SHA256 校對碼。

### 數位簽章

Linux AppImage 以及原始碼的 .tar.gz 和 .tar.xz 壓縮檔已使用數位簽章簽名。你可以從[這裡](https://files.kde.org/krita/4DA79EDA231C852B)取得 GPG 公鑰。簽名檔可於[此處](https://download.kde.org/stable/krita/5.0.6/)找到（副檔名為 .sig）。
