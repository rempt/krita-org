---
title: "發佈 Krita 5.1.0 發行候選版本 (RC1)"
date: "2022-08-04"
categories: 
  - "news_zh-tw"
  - "development-builds_zh-tw"
---

今天我們發佈了 Krita 5.1.0 的首個發行候選版本 (RC1)。

想知道完整的新功能清單的話，可以看看還未寫完的[英文版發佈通告](https://krita.org/en/krita-5-1-release-notes/)喔！

您也可以觀看以下由 Wojtek Trybus 製作的更新內容影片：

{{< youtube  TnvCjziCUGI >}}

 

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是自由、免費及開源的專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或[購買教學影片](https://krita.org/en/shop/)支持我們吧！得到您們的熱心支持，我們才能夠讓核心開發者全職為 Krita 工作。

注意：Krita 5.1 Beta 1 測試版本中含有一個程式錯誤，或會導致筆刷預設儲存出錯，使筆刷在其他 Krita 版本中載入時出現軟體崩潰。如您曾使用 Beta 1 製作或修改筆刷預設，[這份 Python 指令稿](https://invent.kde.org/tymond/brushes-metadata-fixer)可為您修正這些筆刷預設。

在發佈 [Beta 2](https://krita.org/zh-tw/item/krita-5-1-beta-2_zh-tw/) 之後，我們修正了以下問題： （譯者按：由於內容眾多而人力資源有限，故此段落不作完整翻譯，保留英文原文。）

- Several issues with renaming existing and system tags for resources have resolved. [BUG:453831](https://bugs.kde.org/show_bug.cgi?id=453831)
- Python: make it possible to get all pattern resources.
- Fix a slowdown when attaching a dynamic sensor to the ration option in the brush preset editor. [BUG:456668](https://bugs.kde.org/show_bug.cgi?id=456668)
- Fix scaling of the colorspace selector when display scaling. [BUG:456929](https://bugs.kde.org/show_bug.cgi?id=456929)
- Fix a memory leak in the storyboard docker. [BUG:456998](https://bugs.kde.org/show_bug.cgi?id=456998)
- Fix a memory leak in the tile engine. [BUG:456998](https://bugs.kde.org/show_bug.cgi?id=457998)
- Make the Alpha mode the default generator for the Halftone filter.
- Improve handling of switching between exporting just the frames or just the video for an animation. [BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- Clean up files when exporting an animation to frames is canceled. [BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- Fix the Elliptic are to Bezier curve function in SVG so certain files can be loaded without Krita running out of memory. [BUG:456922](https://bugs.kde.org/show_bug.cgi?id=456922), [BUG:439145](https://bugs.kde.org/show_bug.cgi?id=439145)
- Fix issues handling gradient fills for vector objects. [BUG:456807](https://bugs.kde.org/show_bug.cgi?id=456807)
- Make sure the mimetype selector Ok and Cancel buttons can be translated. [BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- Fix a crash when cloning a document through the scripting API. [BUG:457080](https://bugs.kde.org/show_bug.cgi?id=457080)
- Reset the file path when creating a copy of an existing image. [BUG:457081](https://bugs.kde.org/show_bug.cgi?id=457081)
- Fix a crash when copy-pasting a group with vector layers. [BUG:457154](https://bugs.kde.org/show_bug.cgi?id=457154)
- Fix saving a image with a text object to PSD. [BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- Fix a crash when undoing creating a text shape. [BUG:457125](https://bugs.kde.org/show_bug.cgi?id=457125)
- Fix a crash when loading a thumbnail for a PSD file with embedded resources. [BUG:457123](https://bugs.kde.org/show_bug.cgi?id=456123)
- Improve the performance of creating a thumbnail from a PSD file for the recent files list. [BUG:456907](https://bugs.kde.org/show_bug.cgi?id=456907)
- Fix loading PSD files with embedded patterns.
- Fix undeleting tags that have been removed. [BUG:440337](https://bugs.kde.org/show_bug.cgi?id=440337)
- Make it possible to open some invalid PSD files. [BUG:444844](https://bugs.kde.org/show_bug.cgi?id=444844)
- Fix positioning of layers returned from the G'Mic plugin. [BUG:456950](https://bugs.kde.org/show_bug.cgi?id=456950)
- Fix loading JPEG-XL images created by Substance Designer. [BUG:456738](https://bugs.kde.org/show_bug.cgi?id=456738)
- Fix showing the alpha channel in the color picker tool options widget.
- Fix a crash when there is a corrupt in image on the clipboard. [BUG:456778](https://bugs.kde.org/show_bug.cgi?id=456778)
- Fix the positioning when pasting multiple reference images. [BUG:456382](https://bugs.kde.org/show_bug.cgi?id=456382)
- Fix Krita hanging when double-clicking too fast with the contiguous selection tool. [BUG:450577](https://bugs.kde.org/show_bug.cgi?id=450577)
- Fix updating the current palette when selecting a color with the color picker. [BUG:455203](https://bugs.kde.org/show_bug.cgi?id=455203)
- Improve the calculation of the brush speed sensor. [BUG:453401](https://bugs.kde.org/show_bug.cgi?id=453401)

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.0-RC1-setup.exe](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.0-RC1.zip](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-dbg.zip)

### Linux

- 64 位元 Linux：[krita-5.1.0-RC1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.0-RC1.dmg](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.dmg)

### Android

由於 Android SDK 軟體需求變更的影響，目前我們暫時無法建置 Android APK 軟體包。我們會於發佈正式版時恢復提供 Android 版本的 APK。

### 原始碼

- [krita-5.1.0-RC1.tar.gz](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.gz)
- [krita-5.1.0-RC1.tar.xz](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.xz)
